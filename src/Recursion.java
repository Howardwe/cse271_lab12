/*
 * This class is for static helper methods that use recursion.
 * 
 * @Author William Howard
 */
public class Recursion {
    
    /*
     * Given base and n that are both 1 or more, 
     * compute recursively (no loops) the value 
     * of base to the n power, so powerN(3, 2) 
     * is 9 (3 squared).
     * @param base The number that is to be multiplied 
     *     against itself n times.
     * @param n The number of times to multiply base by itself.
     * @return The result of the mathematical exponentiation.
     */
    public static int powerN(int base, int n) {
        //break case
        if (n <= 0) {
            return 1;
        }
        //the main math and recursion
        return base * powerN(base, n - 1);
    }

    /*
     * We have triangle made of blocks. 
     * The topmost row has 1 block, the next 
     * row down has 2 blocks, the next row has 
     * 3 blocks, and so on. Compute recursively 
     * (no loops or multiplication) the total 
     * number of blocks in such a triangle with 
     * the given number of rows.
     * @param row The number of rows the triangle has.
     * @return The number of blocks in the triangle from 
     *     the number of rows.
     */
    public static int triangle(int row) {
        //break case
        if (row <= 0) {
            return 0;
        }
        //the main math and recursion
        return row + triangle(row - 1);
    }

}
